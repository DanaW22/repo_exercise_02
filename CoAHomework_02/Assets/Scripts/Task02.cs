﻿using UnityEngine;
using System.Collections;

namespace CoAHomework02
{
    public class Task02 : MonoBehaviour
    {
        public string PlayerMagic = "Fire";
        public float FlyingHeight = 25.0f;
        public bool IsPlayerManaEmpty = true;
        public int PlayerMana = 350;
        public int SkillPoints = 15;

        public void Start()
        {
            Debug.Log(PlayerMagic);
            Debug.Log(FlyingHeight);
            Debug.Log(IsPlayerManaEmpty);
            Debug.Log(PlayerMana);
            Debug.Log(SkillPoints);
        }
    }
}
