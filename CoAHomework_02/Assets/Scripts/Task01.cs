﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoAHomework02
{
    public class Task01 : MonoBehaviour
    {
        private int GameMode = 2;
        private bool IsModeSelected = true;
        private float EnemyStrength = 15
        private string CharacterName = "Alyn";
        private int CharacterStrength = 150;

        public bool IsClassSelected = false;
        public float CharacterClass = 15.20f;
        public string EnemyName = "Caleb";
        public int EnemyHealth = 0;
        public int CharacterHealth = 250;
        

       public void Start()
        {
            Debug.Log(CharacterHealth);
            Debug.Log(IsClassSelected);
            Debug.Log(CharacterClass);
            Debug.Log(EnemyName);
            Debug.Log(EnemyHealth);
            Debug.Log(GameMode);
            Debug.Log(IsModeSelected);
            Debug.Log(EnemyStrength);
            Debug.Log(CharacterName);
            Debug.Log(CharacterStrength);
        }

        
    }
}
