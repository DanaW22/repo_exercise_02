﻿using UnityEngine;
using System.Collections;

namespace CoAHomework02
{
    public class Task03 : MonoBehaviour
    {
        private int Age = 25;
        private string Name = "Dana";
        private bool IsHomeworkDone = true;

        public void NumberOne()
        {
            
        }

        public void Awake()
        {
            NumberOne();
        }

        public void Update(float a, long b, short c)
        {
            
        
        }

        public void NumberTwo(int d, string e, bool f)
        {
            d = Age;
            e = Name;
            f = IsHomeworkDone;
        }

        public int UniLife(int Age)
        {
            return Age;
        }

        public bool CodingIsFun(bool IsHomeworkDone)
        {
            return IsHomeworkDone;
        }

        public void Start()
        {
            NumberTwo(25,"Dana",true);
            var WhenDidUniLifeStart = UniLife(Age);
            Debug.Log(WhenDidUniLifeStart);
            var Homework = CodingIsFun(IsHomeworkDone);
            Debug.Log(Homework);

        }
    }
}
